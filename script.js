function createNewUser(name, lastName) {
  name = prompt("Ваше ім'я?");

  lastName = prompt("Ваша фамілія?");

  const newUser = {
    get name() {
      return name;
    },
    set name(newName) {
      name = newName;
    },

    get lastName() {
      return lastName;
    },
    set lastName(newLastName) {
      lastName = newLastName;
    },

    getLogin: function () {
      return this.name[0] + this.lastName;
    },
  };

  const login = newUser.getLogin();
  console.log(login);
}

createNewUser();
